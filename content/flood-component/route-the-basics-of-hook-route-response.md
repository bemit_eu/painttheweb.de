#Basics of Hook Route Response

Routes are the basics of a working web app, these route a request to a action, which will somehow respond to the client.

In Flood there are a few things that need to be known about routes and the general execution logic.

## Hooks

`hook`s are bundled actions, they could be understand as small, logically grouped parts of a web app. These could be the frontend and the backend of you app, one app and two separated view and logical parts used.

For each hook there could be registered multiple locales, for each locale you could then set the basic rules, like: host, path-slug, SSL, www/trailing slash and more.

The locale `*` must exist at every hook, these defines basics that will be inherited to each locale in the hook.

Each locale then only need to have a `structure`, `uri`, everything else is optional.

### Example

Locale as path requirement, with `*` as default/landingpage, factory:

```json
{
  "<hook-id>": {
    "route": {
      "hook": {
        "*": {
          "uri": {
            "hostname": "example.org",
            "path": "/"
          },
          "structure": "/",
          "ssl": true,
          "www": false,
          "trailing-slash": false,
        },
        "en-US": {
          "uri": {
            "hostname": "example.org",
            "path": "demo"
          },
          "structure": "/{hook-path}/{locale-path}/{*}",
          "locale-path": "en"
        },
        "de-DE": {
          "uri": {
            "hostname": "example.org",
            "path": "beispiel"
          },
          "structure": "/{hook-path}/{locale-path}/{*}",
          "locale-path": "de"
        }
      }
    },
    "response":{
      "*": {
        "namespace": "Flood\\Hydro\\App\\",
        "register-autoload": true,
        "controller-path": "hook/demo/Controller",
        "type": "object",
        "call-list": [
          "call",
          "handle",
          "response"
        ]
      },
      "home": {
        "*": "/",
        "de-DE": "/",
        "en-US": "/",
        "class": "Home"
      },
      "blog": {
        "de-DE": "blog/(path-dynamic}",
        "en-US": "blog/(path-dynamic}",
        "class": "Blog"
      },
      "contact": {
        "de-DE": {
          "structure-child": "kontakt",
          "class": "Contact"
        },
        "en-US": {
          "structure-child": "contact",
          "class": "Contact"
        }
      }
    }
  }
}
```

de-DE:

- `https://example.org/beispiel/de`
 - `/{hook-path}/{locale-path}/`
 - `$controller = new \Flood\Hydro\App\Home()`
 - `$controller->call()`
 - `$controller->handle()`
 - `$controller->response()`
- `https://example.org/beispiel/de/blog/irgend-ein-dynamischer-text`
 - `/{hook-path}/{locale-path}/blog/(path-dynamic}`
 - `$controller = new \Flood\Hydro\App\Blog()`
 - `$controller->call()`
 - `$controller->handle()`
 - `$controller->response()`
- `https://example.org/beispiel/de/kontakt`
 - `/{hook-path}/{locale-path}/kontakt`
 - `$controller = new \Flood\Hydro\App\Contact()`
 - `$controller->call()`
 - `$controller->handle()`
 - `$controller->response()`
 
en-US:

- `https://example.org/demo/en`
 - `/{hook-path}/{locale-path}/`
 - `$controller = new \Flood\Hydro\App\Home()`
 - `$controller->call()`
 - `$controller->handle()`
 - `$controller->response()`
- `https://example.org/demo/en/blog/some-dynamic-text`
 - `/{hook-path}/{locale-path}/blog/(path-dynamic}`
 - `$controller = new \Flood\Hydro\App\Blog()` 
 - `$controller->call()`
 - `$controller->handle()`
 - `$controller->response()`
- `https://example.org/demo/en/contact`
 - `/{hook-path}/{locale-path}/contact`
 - `$controller = new \Flood\Hydro\App\Contact()`
 - `$controller->call()`
 - `$controller->handle()`
 - `$controller->response()`
- Landing Page: https://example.org/demo/en

Locale as domain, with no default/landingpage:

```json
"hook": {
  "*": {
    "structure": "/{hook-path}/{class}/{method}",
    "ssl": true,
    "www": false,
    "trailing-slash": false,
    "code": 301,
    "subdomain": true
  },
  "en-US": {
    "uri": {
      "hostname": "example.org",
      "path": "demo"
    }
  },
  "de-DE": {
    "uri": {
      "hostname": "de.example.org",
      "path": "beispiel"
    }
  }
}
``` 

## Request

A request is a path and domain pair that routes to a hook and will execute something, the response.

