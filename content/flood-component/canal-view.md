# Template Fragments in Flood\Canal

Fragments are small code parts which are written for a dynamic use, some need to get variables.

They should be inserted like:

```twig
{% include 'fragment/some-fragment.twig' with {'key': 'val'} %}
```

- [Google Maps Api](#google-maps-api)

## Google Maps Api

```text
fragment/google-maps.twig
```

Embeds Google Map, could be used with a Google API Key (Google Static Maps API) or Google Maps Embed API.

### Variables

- `google_maps`
    - `type`: `api` or `embed`, switches between api modes
    - `api_key`: Google Api Key, if exists and needed (Google Static Maps Api)
    - `query`: Position query (Google Static Maps Api)
    - `embed`: Embed code, GET param `pb` (Google Maps Embed Api)

### Example, embed:

```twig
{% include 'fragment/google-map.twig' with {
    'google_maps': {
        'type': 'embed',
        'embed': '<your-embed-code>'
    }
} %}
```

## Contact Form /simple

```text
fragment/contact.twig
```

As simple contact form.

### HTML selector Contact Form /simple

- `.canal--contact-form` on the `<form>`
- fields in the order they are defined
- fields are posted with name `contact_form[<id>][<field-key>]`
- each field with `<label>`, if `label` is defined
- `<input|mixed>` with `div` `input-group` around, if `<label>` is existing, label first then input
    - `.canal--contact-form--input-group .input-group`
- optional `place` for `placeholder="{{ val }}"`   

### Variables Contact Form /simple

- `contact_form`: 
    - `id`: unique id, for multiple per-page usage : string
    - `privacy`: true
    - `method`: `POST`
    - `action`: url.generator.generate(`contact`)
    - `field`: input field that should be shown 
        - `name`: 
            - `place`
            - `type`: `text(email|tel|number)|checkbox|textarea|hidden`
            - `req`: `required` | optional
            - `label`: string | optional
        - `email`: 
            - `place`
            - `req` | optional
        - `tel`: 
            - `place`
            - `req` | optional
        - `msg`: 
            - `place`
    - `button`: 
        - `label`: `absenden`

### Example, embed:

```twig
todo
```

## Navigation: Header

```css
.nav-header {}
```

## Navigation: Service

```css
.nav-service {}
```

# All available variables

The `<head>`has some default values, which didn't need not be set but are recommended. You could set them through your controller. The content controller will push those set in a meta content file to the template.

When nested, the childs are array items, with `()` at the end are object methods:

- `head`
    - `title` used for `<title>`
    - `author` used for `<meta name="author"`
    - `description` used for `<meta name="description"`
    - `font` used for `<link href="{{ head.font }}" rel="stylesheet">`
- `meta`
    - `description` used for `<meta name="description"`
    - `lang` used for `<meta name="description"` and as for the `lang` attribute of `<body>`
- `body`
    - `class` array, will be concated with spaces as classes for `<body>`
    - `attr` array, will be concated with spaces as attributes for `<body>`
- `frontend`
    - `debug` bool
   