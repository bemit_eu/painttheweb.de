# Flood Component: Container - overwrite + bind

Installation and basic usage are described in [Flood Component: Container](/flood-component/container).
    
## Intro

This section covers the advanced usage of the container component. We demonstrate different scenarios for using `overwrite`, `overwriteContainer` and `bind`.

Basic signature:

- `overwrite($id, $callable)` - will overwrite the id with the callable, the id could now not registered
- `overwriteContainer($callable)` - will overwrite the container itself
- `&bind($id, &$reference = null, $overwrite = false)` - will return or set an array from/to another container an will establish a connection, everything done to one is also done to the other container's callback

## Overwrite

The method `overwrite($id, $object)` overwrites an already assigned ID with the object and sets the ID to readonly for the method `set($id, $object)`. You could use it to overwrite different classes in the component. Not every class is currently accessible through the container.

```php
<?php
Container::i()->overwrite('some-id', new YourOwnClass());
```

## Overwrite Container

The container could be overwritten with `::overwriteContainer($callback)`, the then assigned object is returned in `i()` instead of a object of `self`. Meaning overwriting the container must be done in the first lines of execution, as it could be that through `i()` the container is bound somewhere else too fast for overwriting after any other execution. 

```php
<?php
Container::i(); // returns object of `Container` 
Container::overwriteContainer(new YourContainer()); // YourContainer should be extended from the normal container class
Container::i(); // returns object of `YourContainer`
```

## Bind

```php
<?php

SomeModule\Container::i()->bind('hook-storage', Container::i()->bind('hook-storage'));
```