# Flood Component: Route - The Container

See [FloodComponent: Container](/flood-component/container) for the main guide of Flood's container logic.
 
## Access

The container is the class `Flood\Component\Route\Container\Container` which has the singleton method `i()` and the `::$container_id` = `FloodRouteContainer`.

```php
<?php
use Flood\Component\Route\Container\Container;
use Flood\Component\Route\Hook\Hook;
use Flood\Component\Route\Hook\Storage;

Container::i()->register('hook-storage', new Storage()); // Style 1
Container::i()->register('hook', function () {
    return new Hook();
}); // Style 2

Container::i()->get('hook-storage')->getHookList(); // executes Flood\Component\Route\Hook\Storage->getHookList()  
$hook = Container::i()->get('hook'); // sets the var to a new instance of Flood\Component\Route\Hook\Hook
```

## Convenience Methods

- `Container`
    - `_hook()` - for `get('hook')`
    - `_hookStorage()` - for `get('hook-storage')`
    - `_hookRouteGenerator()` - for `get('hook-route-generator')`
    - `_cache()` - for `get('cache')`
    
Convenience methods have the by default assigned class as `@return` indication in `phpdoc` for a wider IDE support and for providing an easy, uniformed way to access and list default classes (in e.g. auto-completion). 
    