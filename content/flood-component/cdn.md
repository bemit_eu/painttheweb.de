# Flood Component: CDN

The CDN component is responsible for serving files to the client. Most modern pages come with a lot of files, most which must not be accessible from the client for security reasons. But media files and things like CSS and JS must be.

With this component it is possible to disallow access through e.g. mod_rewrite to all files and allow acces to only the needed files through access controlled PHP.

Distributed CDN functionalities are not implemented by default.

Installation:

```text    
composer require flood/component-cdn
```