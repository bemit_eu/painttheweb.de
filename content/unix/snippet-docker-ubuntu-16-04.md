# Install Docker

Setup your system

```bash
apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
```

Add Docker repo's GPG keys:

```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

Verify those:

```bash
apt-key fingerprint 0EBFCD88
```

<small>(key could be another, see output of curl)</small>

Setup Docker stable repo and install <a href="https://www.docker.com/community-edition" target="_blank" rel="nofollow">Docker CE</a>.

```bash
sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"

apt-get update

apt-get install docker-ce
```