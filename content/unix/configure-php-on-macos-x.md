# Configure PHP on macOS X

This short entry will show you an easy way of updating, configuring and using the built-in PHP on macOS, tested on High Sierra.

## Updating

As already mentioned PHP is already installed, the current PHP Version could be determined using `php -v` on the terminal.

If you want to switch on 7.2 you could just execute this command, or install it [manually](https://php-osx.liip.ch/#alt_installation)

```bash
curl -s http://php-osx.liip.ch/install.sh | bash -s 7.2
```

## Make it available at the CLI

Now that we have updated PHP you still get your old version with `php -v`, so we need to update your path.

For that make or edit your `~/.bash_profile` and add the line `export PATH=/usr/local/php5/bin:$PATH`.

## Create local webspace

When using the built in PHP you can just edit `/etc/apache2/other/+php-osx.conf`, this file is automatically loaded.

This scheme could be used for the beginning:

```
#
# Additional PHP Apache directives,
# part of the entropy.ch PHP package for Mac OS X
#
# For more information, go to http://www.entropy.ch/software/macosx/php/
#

LoadModule php7_module        /usr/local/php5/libphp7.so

<IfModule mod_php7.c>

    AddType application/x-httpd-php .php
    AddType application/x-httpd-php-source .phps

    <IfModule mod_dir.c>
        DirectoryIndex index.html index.php
    </IfModule>
</IfModule>
```

Also must be set that apache really listens to the IP/Port. Go to `/etc/apache2/httpd.conf` and search for an entry like `Listen 127.0.0.1:80`. Add the IP's and Port's needed you want that the server will be accessible from.

And now just restart Apache to fetch the new config:

```bash
apachectl restart
```

### Debug vhost Configuration

Syntax Check

```bash
apachectl -t
```

List configured vhosts
```bash
apachectl -S
```

## Install Dev Components and Tools

### Composer

Head to the composer page and fetch your copy of composer like it is described there.

After you got the phar file, move it to the `bin` folder like:

```bash
mv composer.phar /usr/local/bin/
```

Now edit your `~/.bash_profile` and add this line:

```bash
alias composer="php /usr/local/bin/composer.phar"
```