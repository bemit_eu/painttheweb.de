# LXC with LXD on an Ubuntu 16.04 server

See official and other docs and guides:

- <a href="https://linuxcontainers.org/lxc/introduction/" target="_blank">LXC</a>
- <a href="https://linuxcontainers.org/lxd/introduction/" target="_blank">LXD</a>
- <a href="https://www.ubuntu.com/server" target="_blank">Ubuntu</a>
- <a href="https://insights.ubuntu.com/2017/02/14/network-management-with-lxd-2-3/" target="_blank">Ubuntu: Network management with LXD (2.3+)</a>
- <a href="https://insights.ubuntu.com/2016/03/22/lxd-2-0-your-first-lxd-container/" target="_blank">LXD 2.0: Your first LXD container</a>

## Intro

Using the linux container system LXC with LXD extension, the LXD extension adds parts like an easier API for LXC and handles networks a little bit nicer.

Prepare the system

```bash
apt update && apt upgrade -y && \\
apt install htop lxc debootstrap bridge-utils -y && \\
apt install -t xenial-backports lxd lxd-client -y;
```

Before first run, lxd needs to be configured with

```bash
lxd init
```

within this the basic settings are set. Like on what storage pool with what driver the containers should be created and stored.

Also a network bridge could be configured, don't confuse it with `Would you like LXD to be available over the network` - this is about if the LXD API should be available through a defined port from the local network. Don't worry, you can skip the network bridge now and configure it later.

## Creation

### Image

What images of ubuntu are available

```bash
lxc image list ubuntu:
```

which alias names exists for the images

```bash
lxc image alias list ubuntu:
```

and see all images

```bash
lxc image list images:
```

### Launch

Creates first container with ubuntu:16.04 server and starts it

```bash
lxc launch ubuntu:16.04 containername
```

this creates but does not start the container, `init` and `launch` are swappable

```bash
lxc init ubuntu:16.04 containername 
```

When not already started, starts container

```bash
lxc start containername
```

## Execution

Switch into root shell of container

```bash
lxc exec containername -- /bin/bash
```

You can go back to the host with `halt`, this shuts the container down too or simply with `exit`.

## Killing

Hard stop of container

```bash
lxc stop containername
```

Delete the container

```bash
lxc delete containername
```

## Profiles

Show profiles of containers, `default` will be used for all containers, which didn't launch with a specific config.

## Network

<a href="https://lxd.readthedocs.io/en/latest/networks/" target="_blank">LXD Network</a> 

Lists existing LXC network bridges

```bash
lxc network list
```
    
Edit a network profile

```bash    
lxc network edit profilename
```
    
Create a network profile

```bash
lxc network create profilename
```

Attach a network profile to a container profile

```bash    
lxc network attach-profile profilename containerprofilename
```
    
Example

```bash    
lxc network create br0
lxc network edit br0

# add this config or like that
config:
    dns.mode: dynamic
    ipv6.address: none
    ipv4.address: 10.10.10.1/24
    ipv4.nat: true
    ipv4.dhcp: true
description: ""
name: br0
type: bridge
used_by: []
managed: true

lxc network attach-profile br0 default
```
    
    
# Synopsis

```bash
apt update && apt upgrade -y && apt install htop lxc debootstrap bridge-utils -y && apt install -t xenial-backports lxd lxd-client -y;
    
# do not create network within init
lxd init

lxc network list

# create network like in network example

lxc launch ubuntu:16.04 containername

lxc start containername && \\
lxc exec containername -- /bin/bash

exit

lxc stop containername
```

Host Network:

- Workstation IP `192.168.178.94`
- LXC-Host IP `192.168.178.35`
- Gateway `192.168.178.1`
- DNS `192.168.178.1`
    
When the container is running now:

- the bridge `br0` has the IP `10.10.10.1` and ?LXD runs an DNS server on it?
- the container has an IP like `10.10.10.*`
- the container could access the internet through the host gateway
- the container could ping and curl the workstation with it's IP
- the host could ping the container
- the host could ping br0
- the workstation can't ping or curl the container
- the workstation can't ping br0

When `ipv4.address: 10.10.10.1/24` is set to `192.168.178.2/24`:

- the bridge `br0` has the IP `192.168.178.2`
- the container has an IP like `192.168.178.*`
- the container can't access the internet through the host gateway
- the container can't ping and curl the workstation with it's IP
- the host can't ping the container
- the host could ping br0
- the workstation can't ping or curl the container
- the workstation could ping br0

Added `ipv4.routes: 192.168.178.1/24`:

- no changes

When changing `dns.mode: dynamic` to `dns.mode: managed` the container is generally not available from the network, it seems.