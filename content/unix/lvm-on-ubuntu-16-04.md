# LVM on Ubuntu 16.04

LVM is the default volume manager in Ubuntu. It enables easy configurable logical virtual volumes on one or more physical hard drives.

It is already installed on <a href="https://www.ubuntu.com/server" target="_blank">Ubuntu 16.04 Server</a>.

## Configure at installation

The LVM should be configured within the installation setup, just e.g. follow the LVM guided partition agent for usage of LVM on the complete disk. When asked how much memory should be consumed, only use a part of the available storage, snapshots or backups could take a lot of storage too!

## Listing of Existing

Show existing volume groups

```bash
vgs
``` 

Show existing volumes with their groups

```bash    
lvs
```

A example output of `lvs`:

```text
LV     VG      Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
root   some-vg -wi-ao---- 69,39g
swap_1 some-vg -wi-ao----  7,91g
```

- `LV` LogicalVolumes
- `VG` VolumeGroups
- `Attr`
- `LSize` the size available for the volume
- `Pool`
- `Origin`
- `Data%` how much of the possible data is saved, when the volume is a snapshot indicates how much of the snaptshots available data is written - according to some wiki the snapshot is only valid when not written completely

The LV `root` is located at `/dev/some-vg/root`, the other volumes are also there. Default is that `root` contains the mount point `/` of Ubuntu.

## Creation

Create a 50GB volume in a group

```bash    
lvcreate -L 50G -n <volume> <group>
lvcreate -L 50G -n some-lv some-vg
```
    
then create a file-system on the volume

```bash
mkfs.ext4 /dev/<group>/<volume> 
mkfs.ext4 /dev/some-vg/some-lv 
```
    
## Size Adjustment

Will reduce the size of the LV to 5GB

```bash
lvreduce --resizefs -L 5G /dev/<group>/<volume>
lvreduce --resizefs -L 5G /dev/some-vg/some-lv
```

## Snapshots

Create a snapshot

```bash
lvcreate --size 50G --snapshot --name <snapshot-name> /dev/<group>/<volume>
```

Merges the snapshot with the original, the original will be again the state of the snapshot

```bash
lvconvert --merge /dev/<group>/<volume>
```
    
Merges could be made with `/dev/some-vg/root`, just reboot and then it will swap the snapshot with root.
Deletes the snapshot after merge! So don't forget to make a new one.

The merge could need some time to finish. The status can be seen with `lvs -a`
The snapshot name is now in brackets and `origin` is at `Original` and `Data%` shows how much data needs to be moved before finished.