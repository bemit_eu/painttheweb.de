# Flood\Hydro Content Manager

Manages the binding of content and translations to templates, and manages the execution of a [RenderTree](/flood-canal/render-tree) with the final rendering of a block.

For this it relies on JSON files which hold the definition of the different elements, together with their data.

It is accessible through
 - class `\Hydro\ContentManager\ContentManager`
 - Hydro container `Container::_content()`  

## Type Definitions

JSON definition files are the core, having those four basic types:

1. `exec` - execution info of one page (like home, about, .. simply everything)
2. `page` - page types define the basic structure and the needed things for n pages
3. `section` - structures multiple other `section` and `block` elements
4. `block` - the only item with a template and that will be finally rendered 

## Basic Rules and Assumptions

1. Each `exec`ution of a page
    - could be identified by an unique (compound) key
    - has one definition file
    - can extend one page type definition
2. A page (`exec`, `page`) and `section`
    - has a `render-tree`, which is interoperable see the [syntax](/flood-hydro/render-tree)
        - is hierarchical cascaded
        - structures n other `section` elements
        - structures n other `block` elements
3. A `block`
    - is the final render object
    - has a `tpl` index for the path to the template file
        - during rendering the template file will be prefixed with the active site as namespace
        - uses twig's multiple folder per namespace system
    - is rendered from `\Hydro\Content\ContentManager->renderBlock($site, $element_ident, $data)` 
4. `exec`, `page`, `section`, `block`
    - can be called independently
    - should have a `name` and `desc`ription for displaying/as info
    - the id of each element is the relative path to the JSON definition file
    
## Content Repository

Where the information are gathered and where what file need to be.

Content repositories are grouped for sites, a site could be understood as a namespace. For one site there could be multiple content repositories. They implement a first-wins overwriting logic.

When setting up a content repository, use the `Container::_content()->addContentRepo` for setting it up.

Recommended is to add it in your Controller that is extending `Hydro\Controller\ResponseStructure`.

```php
<?php
Container::_content()->addContentRepo(
    '<side-id>',
    Container::_config()->serverPath(true) . 'path/to/definition/root/folder', // content_definition - string
    $this->default['content_repo']['content'], // content_relation callback - to a content relation handler
    $this->default['content_repo']['theme']// theme_relation callback to something that could render the theme and inject the data
);
```

With this you can use the default callbacks that are defined from the framework.

### Folder for Definition Files

The folder structure mirrors the semantic names of each element, but the subdirectories and files may be different.

```text
root-folder/
    
    block/
        <block-id>.json
    
    exec/
        <page-id>/
            <locale>.json
    
    page/
        <page-type-id>.json
    
    section/
        <section-id>.json
        
    site.json
```

For an example there are existing:

- blocks with id
    - `cta/top`
    - `cta/bottom`
    - `intro`
- section with id
    - `article-top`
- page-type with id
    - `simple`
- page-exec with id
    - `dashboard` and locales
        - `en-US`
        - `de-DE`
        
```text
content-definition/
    block/
        cta/
            top.json
            bottom.json
        intro.json
        
    exec/
        dashboard/
            de-DE.json
            en-US.json
    
    page/
        simple.json
    
    section/
        article-top.json
        
    site.json
```

```text
content-definition/block/cta/top.json
content-definition/block/cta/bottom.json
content-definition/block/intro.json
    
content-definition/exec/dashboard/de-DE.json
content-definition/exec/dashboard/en-US.json

content-definition/page/simple.json

content-definition/section/article-top.json
    
content-definition/site.json
```

### File Syntax

All files have a basic syntax that is the same, only some indexes are available at one but not the other elements.

Common values:

- `name` the name that should be displayed, no functional use
- `desc` the description that should be displayed, no functional use

#### Block

Extra values:

- `tpl` string : path to the template, without namespace, the current context `site` will be used as namespace
- `data` todo: that will be passed to the template 
- `content` array : optional : that is available in the template through `{{ content.get() }}`

```json
{
  "name": "intro",
  "desc": "For displaying a headline with intro text",
  "tpl": "content-block/intro.twig",
  "data": {
  
  },
  "content": {
    "intro": "Intro from definition file"
  }
}
```

#### Section

- `render-tree` enabled - [RenderTree](/flood-hydro/render-tree)

```json
{
  "name": "Article: Top",
  "desc": "Top Section with Intro and cta banner",
  "render-tree": {
  }
}
```

#### Page

- `render-tree` enabled - [RenderTree](/flood-hydro/render-tree)

```json
{
  "name": "simple",
  "desc": "Simple Content Page",
  "render-tree": {
  }
}
```

#### Exec Page

- mostly doesn't have a `name` and `desc`
- `render-tree` enabled - [RenderTree](/flood-hydro/render-tree)

```json
{
  "render-tree": {
  }
}
```

## Get and Render

For getting and rendering any element, the `ContentManager` provides the API function `get` which receives a string as request to a wanted element.


### Get API

Syntax of a request:

```text
<namespace>:<element-type>:<element-id>
```

Example
- namespace/site is `hydro-app`
- existing blocks, see [above](#folder-for-definition-fil) example

Get a block:
```text
hydro-app:block:cta/top
hydro-app:block:intro
```
Will return the rendered block.

Get a section:
```text
hydro-app:section:article-top
```
Will return a `RenderTree\Section` object.

Get a page-type:
```text
hydro-app:page:simple
```
Will return a `RenderTree\Page` object.

Get a page-exec:
```text
hydro-app:exec:dashboard
```
Will return a `RenderTree\Page` object.

#### Render the Requested Element

A block will be returned already rendered, nothing more to do, but for `section`, `page` and `exec` it will only return a individual handler, which is extended from a `RenderTree`.

With this handler it is possible to render the full element or just a part of it.

```php
<?php
$section = Container::_content()->get(
    'hydro-app:section:article-top',
    [] // optional overwriting data
);

// renders the complete section
$section->render();

// would only render the part with the render-id `cta-top`, who knows if it is a section or a block
$section->render('cta-top');
```