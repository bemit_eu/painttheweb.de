# User, Group And Role Management

### Password

Every user needs a password, even the master.

For generating the master `pass` you need to generate it using the CLI tools:

```bash
php bin/console auth generateHash <pass>
```

Copy the now displayed phrase and add it to `config/auth.json` at the key `master.pass`.

### Master Auth

#### at CLI
Authorize with adding your secret as the `--master|-m` option.

Like `php bin/console cache del all -m <secret>`, the output will say if it was successful or not.

#### at flow - the normal runtime

// todo

#### in PHP

```php
<?php
// create a user object with the `master` ident
$master_user = Container::_authToken()->new('master');
// call validate with the object of the master user
if (Container::_authToken()->validate($master_user, '<the-secret>')) {
    // going here if valid
    
    // you could set the active user to the master
    Container::_authToken()->setActive($master_user);
} else {
    // going here if NOT validated
}
```