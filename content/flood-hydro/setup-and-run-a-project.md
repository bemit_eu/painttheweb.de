# Setup and Run a Flood\Hydro Project

## Fetch Files

Get everything setup with composer, this uses [Hydro: Bucket](https://bitbucket.org/bemit_eu/hydro-bucket) to make all basic folder and files. And will also install all dependencies needed to run a project.

    composer create-project flood/hydro-bucket . -s DEV
    
Throw the files on your server, with **PHP7.1+** and **SSL**, and the page should now indicate that Flood\\Hydro has been executed.

> Info: Hydro is not available yet.