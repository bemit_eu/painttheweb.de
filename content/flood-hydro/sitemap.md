# Hydro Documentation

Setup
- [Setup and Run a project](/flood-hydro/setup-and-run-a-project)
- [HookFile](/flood-hydro/hook-file)

User
- [Authentication](/flood-hydro/user-authentication)
- [Group and Role Management](/flood-hydro/user-group-and-role-management)

Routing
- [HookFile](/flood-hydro/routing-hookfile)
- [Response](/flood-hydro/routing-response)

Content
- [ContentManager](/flood-hydro/content-manager)
- [RenderTree](/flood-hydro/render-tree)
- [Templating](/flood-hydro/templating)