# Flood\Hydro: RenderTree

In a Render Tree are saved the information of how and what a particular element should display.

Enabled, and thous interoperable, are the elements `exec`, `page` and `section`.

## Syntax of Object

A render-tree object is defined in each element's [definition file](/flood-hydro/content-manager#file-syntax).

```json
{
  "render-tree": {
  }
}
```

Inside of the render-tree are defined all elements which should be executed.

The `render-id` is freely definable, with this you can request anything in a render-tree to render, resulting in a modular way of displaying different parts of a page, but only when they are really needed.

```json
{
  "render-tree": {
    "<render-id>": {
      "target": "<type>:<element-id>"
    },
    "render-id2": {
      "some-id": {
        "target": "<type>:<element-id>"
      },
      "some-id2": {
        "target": "<type>:<element-id>"
      }
    }
  }
}
```

- `<render-id>` must be unique in one dimension
- `target` which element should be executed
    - `<type>` could be `section`, `block`
    - `<element-id>` the id of the item that should be rendered
- could me multidimensional, but only when there is not a `target` in the wrapper