# Hydro Templating

<a target="_blank" href="https://bitbucket.org/bemit_eu/hydro">Flood\Hydro</a> uses Twig as main template language. Most templates are build with Canal\View.

See [Canal Templating](/flood-canal/templating) and [Canal Template Fragments](/flood-canal/template-fragments) for the main documenation on the base template and a lot of automatic logic within those.

How to [install Canal](/flood-canal/setup-and-run-a-project).
How to [install Hydro](/flood-hydro/setup-and-run-a-project).

Pre-installed in Hydro, the Canal\View files could also be installed with:

```text    
composer require flood/canal-view
```

## Configuration

### HookFile

In `config/theme.json` is the base configuration defined.

```json
{
  // bool : if the theme handler should display exceptions
  "debug": true,
  // hash : contains view folder
  "path-view": {
    // index is path, value is namespace
    "hook/demo/view": "demo"
  },
  // twig environment configuration, will be added as is, see https://twig.symfony.com/doc/2.x/api.html#environment-options
  "env-config": {
    "cache": "tpl",
    "debug": true,
    "auto_reload": true
  }
}
``` 

For each hook it is possible to add additional configuration.

After the index `theme` belongs the used configuration:

```json
{

  "<hook-id>": {
    "theme": {
      "path-view": {
        "<some/path>": "<namespace>",
        "vendor/flood/canal-view/src": "canal"
      }
    }
  }
}
```

- `path-view` defines the folders in which the template files are, all namespace folders are with automatic-overwrite

# Additional Values

- i18n
  - get
- content
  - get