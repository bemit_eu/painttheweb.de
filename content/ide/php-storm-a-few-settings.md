# PHP Storm - a few settings for the PHP IDE

## Editor

For surrounding the selected text with braces and quotes, check the following checkmark:

    Preferences> Editor > General > Smart Keys: Surround selection on typing quote or brace.