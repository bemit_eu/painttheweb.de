# UI ControlPanel and Editor for Flood\Canal and Flood\Hydro

// todo

## Input Generation and Definition

### SchemaEditor

The SchemaEditor matches supplied schema's to input fields and handles the saving of those. It is split into `native` and `object`, build upon JSON-Schema (invariant: `"type": "object", "object": "actual-type",`) it supports endless nesting. Where native types are like `string` or `boolean|bool` or `number`. All which are type of `object` are considered dynamic content which needed special logic to interact or to create, like a list of strings (interactions: delete item, add item). 

#### Schema Types

- Native
    - string
- Object
    - StringList
    - TextRich
    - TextRichInline
    - *planned*
        - Image
        - Video
    
This example renders 3 input fields, headline will be `input[type=text]`, intro an rich text editor, intro-list an endless list of input text fields where entries can be added or deleted.

```json
{
  "type": "object",
  "properties": {
    "headline": {
      "type": "string"
    },
    "intro": {
      "type": "object",
      "object": "TextRich"
    },
    "intro-list": {
      "type": "object",
      "object": "StringList"
    }
  }
}
```