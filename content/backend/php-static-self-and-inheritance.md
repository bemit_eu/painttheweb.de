# PHP: Static, Self and Inheritance

In this article I will go through the impact of inheritance on `static` and the keyword `self`.

Static properties and methods are declared with the keyword `static`, they could **not** be accessed with `$this`.

## Properties

Static properties will have the same value everywhere, with this the singleton pattern could be implemented, special counter, pre-caching for one runtime or other possible use-cases. They will not be `null` again <small>(or any other default value)</small> when a new instance of a class is created.

In this example is demonstrated how to declare static properties and how to access them.

```php
<?php

class Demo {
    
    public static $property_a;
    public static $property_b = 'b1';
    
    public function set() {
        static::$property_a = 'a1';
        self::$property_b = 'b2';
    }
    
    public function msg(){
        echo static::$property_a;
        echo self::$property_b;
    }
}

echo Demo::$property_a;
                // null, no output
echo Demo::$property_b;
                // output: b1

$demo = new Demo();
$demo->msg();
                // null, no output
                // output: b1
$demo->set();

echo Demo::$property_a;
                // output: a1
echo Demo::$property_b;
                // output: b2

$demo->msg();
                // output: a1
                // output: b2
```

## Methods

Also static methods aren't bound to a `this` context or something like that. They could not be called from initiated object but rather must be called on their respective class.

```php
<?php

class Demo {
    
    public static function msg(){
    }
}
```

## Impact on Inheritance

The value of a static property is also the same in child classes. This makes the whole thing tricky at first.

Given the case of two classes, `DemoA` and `DemoB`, where b extends a. a has the properties `prop_a` and `prop_b` but b only re-implements `prop_b`.

If you set any value like `DemoA::$prop_a = 'val_a'` you will get the value `val_a` also when accessing like `DemoB::$prop_a`.

When accessing a static property inside of one of the two classes, also the keyword is important, `self` will refer to the class it is written whereas `static` refers to the calling class.

### Example

```php
<?php

class DemoA {
    public static $prop_a = 'a_a_val';
    public static $prop_b = 'a_b_val';
    
    
    public static function msg(){
        echo static::$prop_a . "\r\n";
        echo self::$prop_b . "\r\n";
    }
}

class DemoB extends DemoA {
    public static $prop_b = 'b_b_val';
}

echo DemoA::$prop_a
 . "\r\n";
            // output: a_a_val
echo DemoA::$prop_b
 . "\r\n";
            // output: a_b_val

echo DemoB::$prop_a
 . "\r\n";
            // output: a_a_val
echo DemoB::$prop_b
 . "\r\n";
            // output: b_b_val

DemoA::msg();
            // output: a_a_val
            // output: a_b_val

DemoB::msg();
            // output: a_a_val
            // output: a_b_val - here you see how `self` controls the scope `DemoB` accesses the var
            
DemoB::$prop_a = 'b_a_val';
DemoB::$prop_b = 'b_b_val';

DemoA::msg();
            // output: b_a_val
            // output: a_b_val - as `prop_b` is re-implemented in `DemoB`

DemoB::msg();
            // output: b_a_val
            // output: a_b_val - again self
            
// Case: `prop_a` is re-implemented in `DemoB` instead of `prop_b`

DemoA::$prop_a = 'a_a_val';
DemoA::$prop_b = 'a_b_val';

DemoB::$prop_a = 'b_a_val';
DemoB::$prop_b = 'b_b_val';

DemoA::msg();
            // output: a_a_val - re-implemented in `DemoB`
            // output: b_b_val - `self` but nut re-implemented

DemoB::msg();
            // output: b_a_val - `static` instead of `self`
            // output: b_b_val - not re-implemented
```