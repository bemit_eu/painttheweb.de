# FormantaSass

Frontend framework with configuration based development of frontend projects. Full side-by-side compatibility with whatever other framework is in use.

- en/disable of output
- configurable output namespace
- SCSS file base
- everything that could be used as output is available as mixin
- build for multi-client capability


> Full [inline-documentation](https://help.formanta.bemit.eu/sass-doc) of mixins and variables

## Project Folder Structure

The project holds file for a quickstart within any Sass project.

```text
/

```