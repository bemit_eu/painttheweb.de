# sass-core-easy-grunt-config-copy-task

```bash
npm install grunt-contrib-copy --save-dev
```

```js
let path_composer = 'vendor/';
let dest = '_dev/style/config/for/';

let formanta_sass_core = path_composer + 'bemit/formantasass-core/';

var task = {
    copy: {
        main: {
            src: formanta_sass_core + 'animation/_config.scss',
            dest: dest + '_animation.scss',
            options: {
                process: for_config_copy.analyze(content, srcpath)
            },
        },
    },
};
```