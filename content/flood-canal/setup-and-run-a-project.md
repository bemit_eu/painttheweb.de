# Setup and Run a Flood\Canal Project

## Fetch Files

Get everything setup with composer, this uses [Canal: Structure](https://bitbucket.org/bemit_eu/canal-structure) to make all basic folder and files. And will also install all dependencies needed to run a project.

    composer create-project flood/canal-structure . -s DEV
    
## Setup

### on a webserver

Throw the files on your server, with **PHP7.1+** and **SSL**, and the page should now indicate that Flood\Canal has been executed.

### using build in server

*only for development*

#### with everything setup for dev

Needed:

- NodeJS/NPM: scripting runtime, package manager 
- Yarn: for admin React app
- PHP: backend runtime
- Unix Commands: on Windows, e.g. install Git with overwriting Windows commands, makes `ls`, `cd` available

Run NPM scripts:

```bash
npm install

# for unix
npm run-script start-uni

# for windows
npm run-script start-win
```

- `install` will install all needed dependencies for build tools, developing admin, assets
- `start-*` will start the PHP development server and Admin dev server

#### without dev packages

Needed:

- PHP
- Unixoid OS

Use the starts scripts `system/start.bat` for Windows or `system/start.sh` on Linux for the PHP server for Canal.

- Canal Frontend: http://localhost:25020
- Canal Backend: http://localhost:25020/admin/

Currently Windows is only supported with convenience scripts which utilizes Windows Subsystem Linux, recommended to use Ubuntu as subsystem.

## Important Files and Folders

Files for you to configure:

```text
/flow.php
/_config.php
/_content.php
/_route.php

Advanced Configuration in:
/system/
```

- `/_dev` build pipelines and more tooling, primary for asset files
- `/app` controller for the app
- `/app-service` service classes for the app, lib classes, default controller
- `/asset` asset files for JS, Sass (CSS), Media
- `/data/` generated files, data files
- `/data/out/` only folder directly accessible from the client, build pipelines automatically save here
- `/data/content/` files and folders for content
- `/data/content/` files and folders for content

## more

> Recommended: change `$frontend->host` to your domain.
>
> Now access is limited to this domain, you could set the host also per route.

- [templates and view](/flood-canal/templating) files, learn how you could build your own view system.
- [content management](/flood-canal/content-management) on how meta files and content files are included and which possibilities they provide to you.
- [asset and frontend](/flood-canal/asset-management-for-sass-and-js-files) for more information about preconfigured Grunt, WebPack, Sass and JS.
- [CLI access](/flood-canal/console-cli-mode) how features could be activated through the console and items added or deleted from the storage.

You of cause could just use <a href="https://bitbucket.org/bemit_eu/canal/" target="_blank">`Flood/Canal`</a> without the other projects and make your own `flow.php` and basic template/asset stack.

> Don't forget to add an user through the CLI. [create user](/flood-canal/console-cli-mode#create-user) 