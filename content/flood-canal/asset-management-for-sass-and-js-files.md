# Asset Management for Sass and JS files in Flood\Canal

<a target="_blank" href="https://bitbucket.org/bemit_eu/canal">Flood\Canal</a>'s Structure has preconfigured Buildtools for Sass to CSS, CSS optimizes, JS concating, JS uglify, image optimizes and more.

Since version `0.7.x` these are sorted per tool in `/_dev`. Grunt has been deprecated and Webpack is introduced for ES6 to JS.

The [Canal\Asset](/flood-component/canal-asset) holds asset files, you can build upon those.

## Grunt

In the `/_dev/grunt/GruntFile.js` file. As the canal-structure project is by nature not possible to upgrade with composer, it is recommended to only change the variables or to granular document your changes. That you need to change is needed in most cases and are expected in build tools.

### Grunt Tasks

> Grunt support is dropped from June 2018 on.

In Grunt preconfigured are those tasks, they use variables and default paths:

- `grunt css`: sass to css tasks
- `grunt js`: js to js tasks
- `grunt image`: image to image tasks
- `grunt build`: all tasks
- `grunt watch`: watch for file changes
- `grunt watch:css`: watch only for sass file changes
- `grunt watch:js`: watch only for js file changes
- `grunt watch:image`: watch only for image file changes

and more.

## Default Files

Preconfigured Development files:

```text
/asset/

/asset/js/
/asset/js/src/
/asset/js/execute.js
/asset/js/execute-es6.js

/asset/media/

/asset/style/
/asset/style/_meta.scss
/asset/style/main.scss
```

And here are the files which could be accessed from the client, the only folder not handled with `flow.php`:

```text
/data/out/

/data/out/js.js
/data/out/js.min.js

/data/out/media/

/data/out/style.css
/data/out/style.min.css
```

The files are generated through the grunt tasks and their npm modules.

## Style It!

Before you can change anything, you need to setup the tools, install NodeJS, then:

cli:
```bash
npm install -g node-sass sassdoc grunt-cli bower;
```

cd into your project dir.

```bash
bower install;
npm install;
grunt css;
```

The Sass files where transpiled, this is a check if the npm/grunt dependencies have been installed so far.

Now you could start with frontend development. Just start the task `watch` and  begin coding!

If you need any further frameworks, libraries or whatever included in watching, concating and more see the inline documentation at the variables part of `GruntFile.js`

## Switch to Webpack

So now after a while all Flood/* projects are using Grunt, with having the first parts which, no matter why, need a better JS modularization and re-usability, the switch to ES6/ES7 is inevitable. Introducing webpack for all JS related tasks. 

## Gulp instead of Grunt

The switch is around the corner, we will soon update the boilerplate and documentation.

Grunt support is dropped from June 2018 on. 