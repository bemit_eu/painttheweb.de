# Flood\Canal: Command Line Interface Tools

The CLI tools could be used for automation of your environment.

You just need to open a console, `cd` into the folder, switch to the correct user and type `php cli cache del all` to clear all cache!

## Arguments

With arguments you control what will be executed.

The first argument is `scope` - this defines where the task is registered. The second argument is `task` - which task should be executed. And the last is `something` - the meaning of something varies from task to task, it is mostly a value.

There are 11 optional arguments - registered in the definition as options. Use them in a task to define a lot more arguments, if needed. Those optionals are the `a` tags: `-a, -a0, -a1, -a2, -a3, -a4, -a5, -a6, -a7, -a8, -a9`

> The main language for the CLI API is based on this sentence:
>
> in `scope` execute `task` with `something`
>
> like:
>
> `php ./cli cache del '*'`
>
> `cache del *`
>
> in `cache` execute `del` with `*`
>
> in cache execute the deletion with the path setting *

## Options

With options you could set additional information for the execution that should be done.

- `--debug, -d` turns **on** error output
- `--silence, -s` turns **off** runtime info output
- `--env, -e` default `dev` the runtime environments name

# CLI Recipes

Commands for managing Flood\Canal core components through the cli.

## Cache Deletion

Delete any folder inside of `$frontend->path_tmp` or the whole folder.

Delete some folder, like template cache:

```bash
php cli cache del tpl
```

Delete whole cache folder:

```bash
php cli cache del '*'
```

## User Management

### Activate User Management

```bash
php cli user activate
```

### Deactivate User + Delete Data

```bash
php cli user deactivate
```

### Create User

```bash
php cli user create <name> -a 'pass'
```

### Delete User

```bash
php cli user delete <name>
```

### Update Password

```bash
php cli user updatePass <name> -a 'pass'
```

### Verify / Authenticate Check

```bash
php cli user verify <name> -a 'pass'
```

### User Exists

```bash
php cli user exist <name>
```

### User Info

Get list of users:

```bash
php cli user get '*'
```

Get data of one user:

```bash
php cli user get <name>
php cli user get demo
```

## User Token

### Activate User Token

```bash
php cli user.token activate
```

### Deactivate User Token + Delete Data

```bash
php cli user.token deactivate
```

### New User Token

```bash
php cli user.token new <user-name> -a 'pass'
```

### Verify / Check Authentication

```bash
php cli user.token verify <token> -a 'signature'
```

### Token List & Info

```bash
php cli user.token list
```

### Clean Token

Remove all Token that are not longer valid, e.g. too old.

```bash
php cli user.token clean
```

## Content

Check and create needed files for Article, Section, Block.

### Create Article Files

Creates the meta and tree file which are not existing.

In default it will create files with empty arrays, use `path` to specify default values. The file is a json with indices `meta` and `tree` whichs content will be used for `ArticleMeta` and `ArticleTree` default files. `path` will be read relative to the working directory. 

```bash
php cli content autocreate
php cli content autocreate <path>
php cli content autocreate 'file.json'
```

Example of `file.json`:

```json
{
  "meta": {
    "_schema": "meta",
    "_tree": true,
    "date": {
      "create": "2017-12-13 12:00",
      "update": "2017-12-14 12:00"
    },
    "author": [
    ],
    "head": {
      "title": "",
      "author": ""
    },
    "meta": {
      "lang": "en",
      "description": ""
    }
  },
  "tree": {
    "<fragment>": [
      {
        "type": "meta",
        "data": "head.title"
      },
      {
        "type": "block",
        "id": "intro",
        "data": {
        }
      }
    ]
  }
}
```