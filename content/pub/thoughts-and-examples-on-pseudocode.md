# Thoughts and Examples on Pseudocode

## Intro

Pseudo and code, it is code that is logical for you and could work if there would exist such a language.

What? Yes, you invent pseudocode and it will be your own language! Great?

But, the language is not implemented in any system? It does not work <small>(most times)</small>. Why then bother about it, or having it at all?

Because it is great for communication, bug reports, documentation, guides, helping other even you don't know there language and a lot more!

## Which skills do I already need to have?

To truly understand the meaning and possibilities of pseudocode it is recommended to have experience in two or more languages. You then found it useful to have a few common language dialects which are interoperable, even if it is only for you.

## Case: PHP, JSON and SQL

So given as example you speak PHP, JSON and some SQL you want to take some notes about arrays, indices and what will be saved or read from which column of which table.

These three language got a lot in common, for example:

- using names to identify something
- that something could be different things
- these things could be defined by various flags like `string`, `table`, `column`, `array`, `object`
- flags have a true meaning in the coding world but could differ from language to language - `float` in PHP is a `double` in C 

To define the basics of storing values with indices, this concept we now use in simplified snippets:

with MySQL table:

```sql
CREATE TABLE 
  `river_list` (
    `name` VARCHAR(255),
    `length` VARCHAR(255),
     UNIQUE (`name`)
  );
  
INSERT INTO `river_list` (`name`, `length`) VALUES ('rhine', '1233 km');

SELECT `length` FROM `river_list` WHERE `name` = `rhine`;
# result: 1233 km
```

with JSON object:

```json
{
  "river-list": {
    "rhine": {
      "length": "1233 km"
    }
  }
}
```

with PHP array:

```php
<?php
$river_list = [
    'rhine' => [
        'length' => '1233 km',
    ]
];

echo $river_list['rhine']['length'];
// prints: 1233 km
```

So very simple, now you want do describe that structure, this could just be a note for yourself, describing in a documentation where someone could access the length but your project saves it on multiple points but with the same logical structure. 

#### Sentence like method


> The length for a river is saved in each element of 'river-list', each element indice is the rivers name and unique ident.


But it could be hard to understand if you will now think about project life cycle points like migrations and moving things from one to another folder or comparing things. Multiple of those sentences would often need to be combined to describe it.

#### Real code method

You will use real code examples, and that readers understand it correctly you need to add your real project code there, but that means you need to hold it up to date completely or you need to select just some line instead of everything. On some use cases this is the best, but often you already use some kind of pseudo code in the describing comments. If you write documentation at all, you know who I mean.

#### Describing it with pseudocode

Now! Finally! Pseudocode:

```pseudo
rhiver-list.<river-name>.length
```  

That's it!

## Why have I read this?

With writing pseudocode or using it you will begin to code in a new way, you will see coding languages more like real language you speak with a machine. Through helping you to better see the basic concepts and using them in a grammar and fluent way in the every day coding.