# FOOS - Fragment Object Object State, a Schemata for HTML and Sass
# Layout Fragment Block Object State, a Schemata for HTML and Sass
## building scalable, reusable frontend components

Organizing HTML and CSS in a big library or platform that contains many and many components does need a lot of effort to keep it clean and separated and out of death code.  

FOOS is a schema, not methodology or cult, just a plain schema for how it is possible to build a selector architecture and html nesting that enables you to easier handle those tasks.

In the examples are used Sass, CSS and HTML, for a easier writing Sass is recommended, the basics could be used in CSS, advanced schemata could only be used with Sass or a preprocessor with Mixins/Placeholder capability. JS Notation is used for describing some nesting or type.

## Introduction

At first separate three basic scopes of HTML on you page:

- Layout: ground structuring, these could be your Header, Center, Footer on a page 
- Blocks: contained in one layout, like separating your header in two parts 'top' and 'bottom', blocks could be nested in each other 
- Fragments: everything is it: layout and blocks too. every fragment is the root `Node` of something, it can - but most not be - unique, it

### Fragments



### Object Object

