# Content Driven or Domain Driven Design for Web

Why do people go on most web-pages? To find content!

Everything else is more decoration then needed. In this article I will discuss some aspects of why design and functionality doesn't matter as much as the content. Principles of domain-driven-design - an approach for modelling complex software architectures - could also be applied on the conception of a homepage. From a simple business-card like homepage to a big page where a visitor could also order something or use services.

This post provides an overview of the needed steps, but will not go into detail of how the steps are performed. It will reflect how I work in different teams on software development and agency services.

## How we differentiate Content Driven vs. Domain Driven

Both have a lot in common, we see content driven as a model for pages where already a lot of content is existing and should be displayed with additional informations. Whereas domain driven is better for pages where none, or minimal, content is already existing and should be displayed with other information and services. Having both in mind could reflect in a better understanding of the type the page and the resulting needed implementation.

## What is Content Driven Design?

On content driven design, the content is king, this approach could be used on most places for an initial gathering of information, if the intention is some purely content displaying page it is mostly perfect for the whole planning. This depends on the company/person and what content will be existing.

### Example: CDD

As an example we will use Kim, a cooking enthusiast with her own small catering.

At first we will see what content is already existing:

- recipes for dinner
- baking recipes, sweet things and hearty things
- tips for baking and cooking
- tips for growing own herbs

Then we will discuss what she plans to produce:

- tips for a healthier lifestyle
- recipes with general cooking
- information about where her catering could be booked
- no information about when she is free to be booked

As you see it is also helpful to know what is not desired or is simply to much for the budget.

After this we already know a lot of the wanted display types, with this we need to abstract the information and structure that would be helpful.
 
The first step could be to abstract the basic content types:

- recipes
- tips
- catering information

Building on the content types the needed categories or sub-types could be found, this doesn't mean those would be reflected in the menu. We could also mark content that matters the most for the customer - not the visitor, those a mostly those where the most content will be existing.

- recipes
    - **dinner**
    - baking
        - **sweet**
        - hearty
    - cooking
- tips
    - baking
    - cooking
    - growing herbs
    - healthier lifestyle
- information
    - catering service display
    
Now after knowing which types are needed we should find the content information for each of those, this could be done per group or also for sub-types, when they have something special. Some contents can only exist if the customer have them or want to produce them. The initial listing could be done alone but need to be refined with the customer.

- recipes
    - listing of ingredients
    - information about duration and complexity
    - how it is done
    - images and/or videos of the endresult
    - images and/or videos who go along with the 'how it is done'
    - displaying meals that would match with the current
- tips
    - simple blog like content with
        - text
        - images
        - videos
    - related tips
- information
    - contact possibility
    - about where she is available
    - about what she offers
    - about her references
    
After this step, the first wireframe for the types could be done and the design and development could begin.

> This is not the last step, but this example should have reflected a good introduction for the first steps and how the information could be transformed step for step into the product that fits perfectly for the content.

## What is Domain Driven Design?

Domain driven design is a much more granular and complexer approach, depending on budget, complexity and customer/target-client it need to be done with a lot persons. The primary statement is: the domain of the project needs to be understood as deep as possible for building something good. As of this: a designer or developer on it's own can not understood it enough, the customer or domain-expert need to be the primary source of information and be involved in the evolving of the product - from beginning until release and the next iterations - most of those projects need to be adjusted continuously to fit.

This is not a intro into domain-driven-design, as this would need a few pages on it's own. Here are some concepts:

- the need of domain experts
- people who build the product need to be open to know a lot of new stuff
- creative exchange in all stages between all people
- it doesn't suffices to have a rough understanding


I, personally, always integrate design-thinking concepts into domain-driven-design - they just match perfectly.

## Example: DDD
