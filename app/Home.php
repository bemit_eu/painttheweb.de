<?php

namespace Painttheweb;

class Home extends Service\View {
    /**
     * @param \Painttheweb\Service\Frontend $frontend
     */
    public function __construct($frontend) {
        parent::__construct($frontend);
        $this->assign('head', [
            'title'  => 'Home - Paint the Web',
            'author' => 'Michael Becker',
        ]);

        $this->assign('meta', [
            'description' => 'The web is big, endless, scary, moody and wonderful - on Paint the Web you will read all different things that have any connection to web development, design, technology and a few things on management and projects.',
        ]);

        // switches if hidden articles should be displayed on the listing
        $hidden = false;
        if (filter_has_var(INPUT_GET, 'hidden')) {
            // if get param hidden is set
            if (true == ($tmp = filter_input(INPUT_GET, 'hidden', FILTER_SANITIZE_STRING))) {
                // if get param hidden is a value convertible to true (not identical, only equal) and that represents `true`
                $hidden = true;
            }
        }
        $this->assign('blog_article_list', $this->frontend->blog->getSorted($hidden));
    }

    public function response() {
        return $this->render('Home.twig');
    }
}