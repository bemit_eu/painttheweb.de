<?php

namespace Painttheweb;


class Sitemap extends Service\View {

    /**
     * @param \Painttheweb\Service\Frontend $frontend
     */
    public function __construct($frontend) {
        parent::__construct($frontend);
        $this->assign('head', [
            'title' => 'Home',
            'author' => 'Michael Becker',
        ]);

        $this->assign('blog_article_list', $this->frontend->blog->getSorted());
    }

    public function response() {
        return $this->render('Sitemap.twig');
    }
}