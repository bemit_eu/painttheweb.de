<?php

namespace Painttheweb;


class Blog extends Service\FactoryMd {

    /**
     * @param \Painttheweb\Service\Frontend $frontend
     */
    public function __construct($frontend) {
        parent::__construct($frontend);
    }

    /**
     * @param string $template path to template file, when null used FactoryMd.twig
     * @param string $file     relative file of content .md withing /content/
     *
     * @return string
     */
    public function response($template = null, $file = null) {
        if (null !== $template) {
            return parent::response($template, $file);
        } else {
            return parent::response('Blog.twig', $file);
        }
    }
}