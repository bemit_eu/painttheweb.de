<?php

namespace Painttheweb;


class Law extends Service\FactoryMd {

    /**
     * @param \Painttheweb\Service\Frontend $frontend
     */
    public function __construct($frontend) {
        parent::__construct($frontend);
        $this->addHeader('X-Robots-Tag: noindex, nofollow');
        $this->assign('meta', [
            'lang' => $this->frontend->match['locale'],
        ]);
    }

    /**
     * @param string $template path to template file, when null used FactoryMd.twig
     * @param string $file     relative file of content .md withing /content/
     *
     * @return string
     */
    public function response($template = null, $file = null) {
        if (null === $file) {
            $file = 'law/' . $this->frontend->match['locale'] . '_' . $this->frontend->match['page'] . '.md';
        }

        if (null !== $template) {
            return parent::response($template, $file);
        } else {
            return parent::response('Law.twig', $file);
        }
    }
}