<?php

namespace Painttheweb;


class Rss extends Service\View {

    /**
     * @param \Painttheweb\Service\Frontend $frontend
     */
    public function __construct($frontend) {
        parent::__construct($frontend);
        $this->assign('head', [
            'title' => 'Home',
            'author' => 'Michael Becker',
        ]);

        // todo check why not all not-hidden articles are fetched
        $this->assign('blog_article_list', $this->frontend->blog->getSorted());
    }

    public function response() {
        return $this->render('Rss.twig');
    }
}