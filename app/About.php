<?php

namespace Painttheweb;

class About extends Service\View {

    /**
     * @param \Painttheweb\Service\Frontend $frontend
     */
    public function __construct($frontend) {
        parent::__construct($frontend);
        $this->assign('head', [
            'title' => 'About',
            'author' => 'Michael Becker',
        ]);
        $this->assign('meta', [
            'description' => 'a micro-Blog in .md about Dev, Web and more - find Guides, Comments, Code, Mind-Hangouts and more in a nice and compact way.',
            'lang' => 'en',
        ]);
    }

    public function response() {
        return $this->render('About.twig');
    }
}