<?php

namespace Painttheweb;

class Projects extends Service\View {

    /**
     * @param \Painttheweb\Service\Frontend $frontend
     */
    public function __construct($frontend) {
        parent::__construct($frontend);
        $this->assign('head', [
            'title' => 'Projects',
            'author' => 'Michael Becker',
        ]);
        $this->assign('meta', [
            'description' => 'On which projects is worked? Web development projects for frontend and backend, open source.',
            'lang' => 'en',
        ]);
    }

    public function response() {
        return $this->render('Projects.twig');
    }
}