<?php

namespace Painttheweb;

class Four04 extends Service\View {
    public static $four04_rendered = false;

    /**
     * @param \Painttheweb\Service\Frontend $frontend
     */
    public function __construct($frontend) {
        parent::__construct($frontend);
        static::$four04_rendered = true;
        $this->assign('head', [
            'title'  => 'NULL - Paint the Web',
            'author' => 'Michael Becker',
        ]);

        $this->assign('meta', [
            'description' => '',
        ]);

        $this->assign('blog_article_list', $this->frontend->blog->getSorted(true));
    }

    public function response() {
        header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');

        return $this->render('Four04.twig');
    }
}