<?php

namespace Painttheweb\Service;

use Symfony\Component\DomCrawler\Crawler;

/**
 * Class Markdown
 *
 * @package Painttheweb\Service
 */
class Markdown {
    /**
     * @var \Parsedown
     */
    protected $parsedown;

    protected $crawler;

    /**
     * Markdown constructor.
     */
    public function __construct() {
        $this->parsedown = new \Parsedown();
        $this->crawler = new Crawler();
    }

    /**
     * @param $text
     *
     * @return string
     */
    public function text($text) {
        return $this->parsedown->text($text);
    }

    /**
     * @param $path
     *
     * @return bool|string
     */
    public function textFile($path) {
        if (false !== ($file_content = file_get_contents($path))) {
            return $this->text($file_content);
        } else {
            return false;
        }
    }

    /**
     * Could be used for generating a index of table, when not done through JS
     *
     * @param $text
     */
    public function crawl($text) {
        $this->crawler->addContent($text);
    }

    /**
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function getH1() {
        return $this->crawler->filter('h1');
    }

    /**
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function getH2() {
        return $this->crawler->filter('h2');
    }

    /**
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function getH3() {
        return $this->crawler->filter('h3');
    }

    /**
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function getH4() {
        return $this->crawler->filter('h4');
    }

    /**
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function getH5() {
        return $this->crawler->filter('h5');
    }

    /**
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function getH6() {
        return $this->crawler->filter('h6');
    }
}