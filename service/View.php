<?php

namespace Painttheweb\Service;


class View {

    /**
     * @var \Twig_Environment
     */
    protected $tpl;
    /**
     * @var array
     */
    protected $tpl_data;

    /**
     * @var Markdown
     */
    protected $md;

    /**
     * @var \Symfony\Component\Routing\RequestContext
     */
    protected $context;

    /**
     * @var \Painttheweb\Service\Frontend
     */
    protected $frontend;

    protected $header = [];

    /**
     * @param \Painttheweb\Service\Frontend $frontend
     */
    public function __construct($frontend) {
        $this->frontend = $frontend;
        $this->context = $frontend->request_context;
        $twig_loader = new \Twig_Loader_Filesystem(__DIR__ . '/../view');

        $config = [
            'cache' => __DIR__ . '/../tmp/tpl',
            'debug' => true,
        ];

        $this->tpl = new \Twig_Environment($twig_loader, $config);

        // make dump() and more available
        $this->tpl->addExtension(new \Twig_Extension_Debug());

        $this->md = new Markdown();

        $this->assign('md', $this->md);
        // Is request context and not request!
        $this->assign('request', $this->context);
        $this->assign('blog', $frontend->blog);
        $this->assign('match', $frontend->match);
        $this->assign('frontend', $frontend);

        // which sections are displayed in main nav
        $this->assign('menu', [
            'Flood' => [
            ],
            'flood-component',
        ]);

        $this->assign('url', [
            'asset'     => $this->context->getScheme() . '://' . $this->context->getHost() . '/view/out/',
            'home'      => $this->context->getScheme() . '://' . $this->context->getHost() . '',
            'generator' => $frontend->url_generator,
        ]);
    }

    public function sendHeader() {
        foreach ($this->header as $h) {
            header($h, true);
        }
    }

    public function addHeader($header, $as_array = false) {
        if ($as_array) {
            $this->header = array_merge($this->header, $header);
        } else {
            $this->header[] = $header;
        }
    }

    public function assign($key, $val) {
        $this->tpl_data[$key] = $val;
    }

    public function render($template) {
        $this->sendHeader();
        $template_wrapper = $this->tpl->load($template);
        return $template_wrapper->render($this->tpl_data);
    }
}