<?php

namespace Painttheweb\Service;

/**
 * Class Url
 *
 * @package Painttheweb\Service
 */
class Url {

    /**
     * Redirects to a uri with a header code, and adds when wanted additional headers
     *
     * @todo implement additional_header
     *
     * @param      $goto
     * @param int  $header_code
     * @param null $additional_header pre headers are added before header_code and post after ['pre' => [], 'post' => []]
     */
    public function redirect($goto, $header_code = 301, $additional_header = null) {
        $header_code_msg = $_SERVER['SERVER_PROTOCOL'] . ' ';

        $header_code_list = [
            /* Successfull Operation */
            '200' => 'OK',
            '205' => 'Reset Content',
            '206' => 'Partial Content',
            '207' => 'Multi-Status',

            /* Redirection */
            '300' => 'Multiple Choices',
            '301' => 'Moved Permanently',
            '302' => 'Found',
            '307' => 'Temporary Redirect',
            '308' => 'Permanent Redirect',

            /*  Client-Error */
            '401' => 'Unauthorized',
            '403' => 'Forbidden',
            '404' => 'Not Found',
            '408' => 'Request Time-out',
            '410' => 'Gone',
            '418' => 'I\'m a teapot',
            '421' => 'There are too many connections from your internet address',
            '423' => 'Locked',
            '424' => 'Failed Dependency',
            '429' => 'Too Many Requests',
            '451' => 'Unavailable For Legal Reasons',

            /* Server-Error */
            '503' => 'Service Unavailable',
        ];

        $header_code_msg .= $header_code;
        if (array_key_exists($header_code, $header_code_list)) {
            $header_code_msg .= ' ' . $header_code_list[$header_code];
        }

        header($header_code_msg);

        header('Location: ' . $goto);
        exit();
    }
}