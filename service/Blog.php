<?php

namespace Painttheweb\Service;

class Blog {

    public $section_list = [];

    /**
     * Adds a new section with that name, implements a fluent interface
     *
     * @param $id
     *
     * @return \Painttheweb\Service\BlogSection
     */
    public function addSection($id) {
        $this->section_list[$id] = new BlogSection($id, $this);

        return $this->section_list[$id];
    }

    /**
     * Gets the section with the ID, if section is not existing it will create the section
     *
     * @param $id
     *
     * @return \Painttheweb\Service\BlogSection
     */
    public function getSection($id) {
        if (isset($this->section_list[$id])) {
            return $this->section_list[$id];
        } else {
            return $this->addSection($id);
        }
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function existSection($id) {
        return array_key_exists($id, $this->section_list);
    }

    /**
     * @param bool   $hidden if hidden articles should also be fetched
     * @param string $by
     * @param string $direction
     *
     * @return array
     */
    public function getSorted($hidden = false, $by = 'date.update', $direction = 'ASC') {
        $sorted = [];
        foreach ($this->section_list as $section_id => $section) {
            /**
             * @var \Painttheweb\Service\BlogSection $section
             */
            $sorted = $this->sortArticleList($section->article_list, $sorted, $hidden, $by, $direction);
        }

        return $sorted;
    }

    /**
     * Gets all articles of one section sorted by date
     *
     * @param string $section_id
     * @param bool   $hidden if hidden articles should also be fetched
     * @param string $by
     * @param string $direction
     *
     * @return array
     */
    public function getSortedSection($section_id, $hidden = false, $by = 'date.update', $direction = 'ASC') {
        if (isset($this->section_list[$section_id])) {
            return $this->sortArticleList($this->section_list[$section_id]->article_list, [], $hidden, $by, $direction);
        } else {
            return [];
        }
    }

    public function sortArticleList($article_list, $sorted = [], $hidden = false, $by = 'date.update', $direction = 'ASC') {
        foreach ($article_list as $article_id => $article) {
            /**
             * @var \Painttheweb\Service\BlogArticle $article
             */
            if ($hidden && true === $article->hidden()
                || !$article->hidden()
            ) {
                // when hidden should be fetched and element is hidden
                // OR
                // when is visible (this it is when not hidden)
                if (array_key_exists($article->get('date')['update'], $sorted)) {
                    $sort_key = $article->get('date')['update'] . "#" . rand(1000, 9999);
                } else {
                    $sort_key = $article->get('date')['update'];
                }
                $sorted[$sort_key] = [
                    'article'    => $article,
                    'article_id' => $article_id,
                    'section_id' => $article->section->id,
                ];
            }
        }
        switch ($by) {
            case 'date':
                break;
        }
        //var_dump($sorted);
        uksort($sorted, 'strnatcmp');
        //var_dump($sorted);

        if ('ASC' === $direction) {
            $sorted = array_reverse($sorted, true);
        }

        return $sorted;
    }
}