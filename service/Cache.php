<?php

namespace Painttheweb\Service;


use Symfony\Component\Filesystem\Filesystem;

class Cache {

    public $path;
    /**
     * @var string the currently called persistence id
     */
    public $active_id;

    public function exist($id) {
        return is_file($this->path . $id);
    }

    public function idGen($id) {
        return hash(md5, $id);
    }

    public function get($id) {
        return file_get_contents($this->path . $id);
    }

    public function save($id, $data) {
        file_put_contents($this->path . $id, $data);
    }

    public function clean() {
        $fs = new Filesystem();
        $fs->remove($this->path);
    }
}