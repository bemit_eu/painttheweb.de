<?php

namespace Painttheweb\Service;


class BlogSection {

    /**
     * @var array
     */
    public $article_list = [];

    /**
     * @var string
     */
    public $id;

    /**
     * @var \Painttheweb\Service\Blog
     */
    public $blog;

    /**
     * BlogSection constructor.
     *
     * @param                           $id
     * @param \Painttheweb\Service\Blog $blog
     */
    public function __construct($id, &$blog) {
        $this->id = $id;
        $this->blog = &$blog;
    }

    /**
     * Adds a new article to this section, implements a fluent interface for adding more articles
     *
     * @param        $id
     * @param string $folder
     * @param string $file
     *
     * @return \Painttheweb\Service\BlogSection
     */
    public function addArticle($id, $folder = '', $file = '') {
        $this->article_list[$id] = new BlogArticle($id, $this, $folder, $file);

        return $this;
    }

    /**
     * @param $id
     *
     * @return \Painttheweb\Service\BlogArticle
     */
    public function getArticle($id) {
        return $this->article_list[$id];
    }
}