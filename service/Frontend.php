<?php

namespace Painttheweb\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class Frontend
 *
 * @package Painttheweb\Service
 */
class Frontend {

    /**
     * @var array
     */
    public $route_list = [];

    /**
     * @var string
     */
    public $host = '';

    /**
     * @var bool
     */
    public $ssl = true;

    /**
     * @var \Painttheweb\Service\Blog
     */
    public $blog;

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    public $request;

    /**
     * @var \Symfony\Component\Routing\RequestContext
     */
    public $request_context;

    /**
     * @var \Symfony\Component\Routing\Generator\UrlGenerator
     */
    public $url_generator;

    /**
     * @var array contains the result of the match
     */
    public $match;

    /**
     * @var \Symfony\Component\Routing\RouteCollection
     */
    public $routes;

    /**
     * Frontend constructor.
     *
     * @param array                     $route_list
     * @param string                    $host
     * @param \Painttheweb\Service\Blog $blog
     * @param bool                      $ssl
     */
    public function __construct($route_list, $blog, $host = '', $ssl = true) {
        $this->route_list = $route_list;
        $this->blog = $blog;

        if (!empty($host)) {
            $this->host = $host;
        }

        $this->ssl = $ssl;
    }

    /**
     * @todo add route list cache
     *
     * @return mixed
     */
    public function render() {
        $this->routes = new RouteCollection();

        foreach ($this->route_list as $id => $r) {
            if (isset($r['path'])) {
                $this->routes->add($id, new Route(
                    $r['path'], // path
                    (isset($r['defaults']) ? array_merge(['_controller' => $r['controller']], $r['defaults']) : ['_controller' => $r['controller']]), // parameters
                    (isset($r['requirements']) ? $r['requirements'] : []), // requirement
                    (isset($r['options']) ? $r['options'] : []), // options
                    $this->host, // host
                    [($this->ssl ? 'HTTPS' : 'HTTP')], // schemes
                    [] // methods
                ));
            }
        }
        $this->request = Request::createFromGlobals();
        $this->request_context = new RequestContext('/');
        $this->request_context->fromRequest($this->request);
        $matcher = new UrlMatcher($this->routes, $this->request_context);

        $this->url_generator = new UrlGenerator($this->routes, $this->request_context);
        //$this->url_generator->generate();

        if ('/' !== $this->request_context->getPathInfo() && '/' === substr($this->request_context->getPathInfo(), strlen($this->request_context->getPathInfo()) - 1)) {
            // redirect on trailing slash
            // but only when the active path is not only a /, like on https://domain.de/
            $uri = new Url();
            $uri->redirect(str_replace($this->request_context->getPathInfo(), rtrim($this->request_context->getPathInfo(), ' /'), $this->request->getRequestUri()));
        }

        $this->match = $matcher->match($this->request_context->getPathInfo());

        return $this->match['_controller']($this);
    }


    /**
     * Executes render and displays the page
     */
    public function display() {
        echo $this->render();
    }
}