<?php

namespace Painttheweb\Service;


class BlogArticle {

    /**
     * @var array
     */
    public $property = [];

    /**
     * @var string
     */
    public $id;

    /**
     * @var \Painttheweb\Service\BlogSection
     */
    public $section;

    public static $folder_default = '';

    /**
     * BlogArticle constructor.
     *
     * @param                                  $id
     * @param \Painttheweb\Service\BlogSection $section
     * @param string                           $folder
     * @param string                           $file
     */
    public function __construct($id, &$section, $folder = '', $file = '') {
        $this->id = $id;
        $this->section = &$section;

        if (empty($folder)) {
            $folder = static::$folder_default;
        }

        if (empty($file)) {
            $file = $section->id . '_' . $id . '.json';
        }
        $this->property = json_decode(file_get_contents($folder . '/' . $file), true);
    }

    public function get($key) {
        return $this->property[$key];
    }

    public function hidden() {
        if (true === $this->get('hidden')) {
            return true;
        } else {
            return false;
        }
    }
}