<?php

namespace Painttheweb\Service;

use Flood\Component\PerformanceMonitor\Monitor;

/**
 * Class FactoryMd
 *
 * @package Painttheweb
 */
class FactoryMd extends View {

    /**
     * @param \Painttheweb\Service\Frontend $frontend
     */
    public function __construct($frontend) {
        parent::__construct($frontend);
        $this->assign('head', [
            'title'  => ucwords(str_replace(['\\', '/', '-', '_', '.',], [' ', ' ', ' ', ' ', ' ',], $this->context->getPathInfo()) . ' | PaintTheWeb'),
            'author' => 'Michael Becker',
        ]);
        if ($this->frontend->blog->existSection($this->frontend->match['_route'])) {
            if ($this->frontend->blog->getSection($this->frontend->match['_route'])->getArticle($this->frontend->match['article'])) {
                if ($this->frontend->blog->getSection($this->frontend->match['_route'])->getArticle($this->frontend->match['article'])->get('meta')) {
                    $this->assign('meta', $this->frontend->blog->getSection($this->frontend->match['_route'])->getArticle($this->frontend->match['article'])->get('meta'));
                }
            }
        }
    }

    /**
     * @param string $template path to template file, when null used FactoryMd.twig
     * @param string $file     relative file of content .md withing /content/
     *
     * @return string
     */
    public function response($template = null, $file = null) {
        Monitor::i()->startProfile('FactoryMd-response');

        // Try to fetch automatically a file in the folder /content that is named like the path and parse it from MarkDown to HTML
        if (null === $file) {
            $file = __DIR__ . '/../content/' . $this->context->getPathInfo() . '.md';
        } else {
            $file = __DIR__ . '/../content/' . $file;
        }
        if (false !== ($file_content = $this->md->textFile($file))) {
            $this->assign('md_content', $file_content);
            $blog_page_exist = true;
        } else {
            // when page doesn't exist
            $blog_page_exist = false;
        }

        if ($blog_page_exist) {
            Monitor::i()->endProfile('FactoryMd-response');

            if ($this->frontend->blog->existSection($this->frontend->match['_route'])) {
                if ($this->frontend->blog->getSection($this->frontend->match['_route'])->getArticle($this->frontend->match['article'])) {
                    if ($this->frontend->blog->getSection($this->frontend->match['_route'])->getArticle($this->frontend->match['article'])->get('sendHeader')) {
                        $this->addHeader($this->frontend->blog->getSection($this->frontend->match['_route'])->getArticle($this->frontend->match['article'])->get('sendHeader'), true);
                    }
                }
            }
            if (null !== $template) {
                return $this->render($template);
            } else {
                return $this->render('FactoryMd.twig');
            }
        } else {
            $controller = new \Painttheweb\Four04($this->frontend);
            $controller->assign('blog_page', true);
            return $controller->response();
        }
    }
}