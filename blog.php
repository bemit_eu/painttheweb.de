<?php

use Painttheweb\Service\Blog;
use Painttheweb\Service\BlogArticle;

if(!isset($blog)) {
    /**
     * @var Blog $blog will be set from flow.php
     */
    $blog = new Blog();
}

BlogArticle::$folder_default = __DIR__ . '/blog';

//
// Unix, Linux and macOS
// primary server and development environments
//
$blog->addSection('unix')
     ->addArticle('lvm-on-ubuntu-16-04')
     ->addArticle('lxc-with-lxd-on-ubuntu-16-04')
     ->addArticle('lamp-or-lemp-on-ubuntu-16-04')
     ->addArticle('configure-php-on-macos-x');

//
// Frontend Development
//
$blog->addSection('frontend')
     ->addArticle('grunt-quick-start-and-boilerplate');

//
// Backend Development
//
$blog->addSection('backend')
     ->addArticle('php-static-self-and-inheritance');

//
// Pub - the theoretical, mostly language-independent, entries and scripts
//
$blog->addSection('pub')
     ->addArticle('thoughts-and-examples-on-pseudocode')
     ->addArticle('content-driven-and-domain-driven-design-in-web');

//
// Flood
//
$blog->addSection('flood')
     ->addArticle('admin-ui');

//
// Flood Canal
//
$blog->addSection('flood-canal')
     ->addArticle('setup-and-run-a-project')
     ->addArticle('content-management')
     ->addArticle('templating')
     ->addArticle('template-fragments')
     ->addArticle('asset-management-for-sass-and-js-files')
     ->addArticle('console-cli-mode');

//
// Flood Hydro
//
$blog->addSection('flood-hydro')
     ->addArticle('sitemap')
     ->addArticle('setup-and-run-a-project')
     ->addArticle('templating')
     ->addArticle('render-tree')
     ->addArticle('content-manager');

/*->addArticle(
    'content-management',
    __DIR__ . '/blog'
)
->addArticle(
    'asset-management-for-sass-and-js-files',
    __DIR__ . '/blog'
);*/

//
// Flood Components - main
//
$blog->addSection('flood-component')
     ->addArticle('route')
     ->addArticle('route-the-container')
     ->addArticle('container')
     ->addArticle('container-overwrite-and-bind')
     ->addArticle('canal-asset')
     ->addArticle('canal-asset-module-gdpr')
     ->addArticle('cache');

//
// Flood Components - canal
//
$blog->getSection('flood-component')
     ->addArticle('canal-view');


//
// Formanta
//
$blog->addSection('formanta')
     ->addArticle('sass-core')
     ->addArticle('sass');