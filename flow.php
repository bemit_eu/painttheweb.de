<?php

$option['run']['start']['time'] = microtime(true); // Set time value for performance monitor
$option['run']['start']['memory'] = memory_get_usage(); // Set memory value for performance monitor

$option['debug'] = true;

require_once 'vendor/autoload.php';

use Flood\Component\PerformanceMonitor\Monitor;
use Painttheweb\About;
use Painttheweb\Projects;
use Painttheweb\Blog as BlogView;
use Painttheweb\Home;
use Painttheweb\Law;
use Painttheweb\Rss;
use Painttheweb\Service\Blog;
use Painttheweb\Service\Cache;
use Painttheweb\Sitemap;

Monitor::i()->startProfile('all', $option['run']);

$cache = new Cache();
$cache->path = __DIR__ . '/tmp/';
$cache->active_id = $cache->idGen(filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL));

if (
    filter_has_var(INPUT_GET, 'token') &&
    '799b6ecc43ce4cf9371ab3e677bc70b6b42872e55db26d508a238d8af4fb7b15' === hash('haval256,5', filter_input(INPUT_GET, 'token', FILTER_SANITIZE_STRING))
) {
    $cache->clean();
}

if ($cache->exist($cache->active_id)) {
    echo $cache->get($cache->active_id);
} else {
    /**
     * @var string $host is used below to only allow routes from this host - new Frontend(.. $host ..)
     */
    $host = 'painttheweb.de';

    $blog = new Blog();

    require 'blog.php';

    /**
     * @var array $route_list is used to build the symfony routes in Frontend.render()
     */
    $route_list = [
        'home'            => [
            'path'       => '/',
            'controller' => function ($frontend) {
                $controller = new Home($frontend);

                return $controller->response();
            },
        ],
        'about'           => [
            'path'       => '/about',
            'controller' => function ($frontend) {
                $controller = new About($frontend);

                return $controller->response();
            },
        ],
        'projects'        => [
            'path'       => '/projects',
            'controller' => function ($frontend) {
                $controller = new Projects($frontend);

                return $controller->response();
            },
        ],
        'law'             => [
            'path'       => '/law/{page}/{locale}',
            'controller' => function ($frontend) {
                $controller = new Law($frontend);

                return $controller->response();
            },
            'defaults'   => ['rel' => 'nofollow, noindex'],
        ],
        'unix'            => [
            'path'       => '/unix/{article}',
            'controller' => function ($frontend) {
                $controller = new BlogView($frontend);

                return $controller->response();
            },
        ],
        'formanta'        => [
            'path'       => '/formanta/{article}',
            'controller' => function ($frontend) {
                $controller = new BlogView($frontend);

                return $controller->response();
            },
        ],
        'flood'           => [
            'path'       => '/flood/{article}',
            'controller' => function ($frontend) {
                $controller = new BlogView($frontend);

                return $controller->response();
            },
        ],
        'flood-canal'     => [
            'path'       => '/flood-canal/{article}',
            'controller' => function ($frontend) {
                $controller = new BlogView($frontend);

                return $controller->response();
            },
        ],
        'flood-hydro'     => [
            'path'       => '/flood-hydro/{article}',
            'controller' => function ($frontend) {
                $controller = new BlogView($frontend);

                return $controller->response();
            },
        ],
        'flood-component' => [
            'path'       => '/flood-component/{article}',
            'controller' => function ($frontend) {
                $controller = new BlogView($frontend);

                return $controller->response();
            },
        ],
        'frontend'        => [
            'path'       => '/frontend/{article}',
            'controller' => function ($frontend) {
                $controller = new BlogView($frontend);

                return $controller->response();
            },
        ],
        'backend'         => [
            'path'       => '/backend/{article}',
            'controller' => function ($frontend) {
                $controller = new BlogView($frontend);

                return $controller->response();
            },
        ],
        'ide'             => [
            'path'       => '/ide/{article}',
            'controller' => function ($frontend) {
                $controller = new BlogView($frontend);

                return $controller->response();
            },
        ],
        'pub'             => [
            'path'       => '/pub/{article}',
            'controller' => function ($frontend) {
                $controller = new BlogView($frontend);

                return $controller->response();
            },
        ],
        'sitemap-xml'     => [
            'path'       => '/sitemap.xml',
            'controller' => function ($frontend) {
                $controller = new Sitemap($frontend);

                return $controller->response();
            },
        ],
        'rss'             => [
            'path'       => '/rss',
            'controller' => function ($frontend) {
                $controller = new Rss($frontend);

                return $controller->response();
            },
        ],
        '404'             => [
            'path'       => '/{request}',
            'controller' => function ($frontend) {
                $controller = new \Painttheweb\Four04($frontend);

                return $controller->response();
            },
        ],
    ];

    $frontend = new \Painttheweb\Service\Frontend($route_list, $blog, $host);
    $page_ob = $frontend->render();

    if (!\Painttheweb\Four04::$four04_rendered) {
        // cache must only be written when the page really exists, so the cache couldn't be used to crash the server

        // save the rendered page, but compress the html previously
        // from: http://php.net/manual/en/function.ob-start.php#71953

        // but for <code> it doesn't work.. so there need to be some 'not between those html tags'
        /*$page_ob = preg_replace([
            '/\>[^\S ]+/s',     // strip whitespaces after tags, except space
            '/[^\S ]+\</s',     // strip whitespaces before tags, except space
            '/(\s)+/s',         // shorten multiple whitespace sequences
            '/<!--(.|\s)*?-->/' // Remove HTML comments
        ], ['>', '<', '\\1', '',
        ], $page_ob);

        $result = preg_replace(
            '/\s+     # Match one or more whitespace characters
                    (?!       # but only if it is impossible to match...
                    [^<>]*    # any characters except angle brackets
                    >         # followed by a closing bracket.
                    )         # End of lookahead
                    /x',
            '', $subject);*/
        $cache->save($cache->active_id, $page_ob);
    }

    echo $page_ob;
}
Monitor::i()->endProfile('all', $option['run']);
// Monitor::i()->getInformation('all')['all']['time'];
/*if ('sitemap-xml' !== $frontend->match['_route']) {
    echo '<span style="color:#3c3d41;">' . Monitor::i()->getInformation('all')['all']['time'] . '</span>';
}*/

error_log("\r\n" . 'PTW Performance: ' . Monitor::i()->getInformation('all')['all']['time'] . 's ' . Monitor::i()->convertMemory(Monitor::i()->getInformation('all')['all']['memory']) . "\r\n");