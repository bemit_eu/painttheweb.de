/**
 * Makes out of the first 15 chars of an headline the corresponding id, and add the headline to the sticky menu
 *
 * @constructor
 */
function AnchorMaker() {
    this.sticky = null;
    this.sticky_toggle = null;
    this.headline = ['h1', 'h2', 'h3', 'h4', 'h5'];
}

AnchorMaker.prototype.exec = function () {
    this.sticky = $('#nav-sticky');
    this.sticky_container = $('.nav-sticky--container', this.sticky);
    this.sticky_toggle = $('.nav-sticky--toggle', this.sticky);
    this.makeNav();
    this.bindHandler();
    var width = $(window).width();
    if (width >= 1700) {
        this.toggle();
    }
};

AnchorMaker.prototype.bindHandler = function () {
    $(this.sticky_toggle, this.sticky).on('click', this.toggle.bind(this));
};

AnchorMaker.prototype.toggle = function () {
    if (this.sticky_container.hasClass('visible')) {
        this.sticky_container.removeClass('visible');
        this.sticky_toggle.removeClass('visible');
    } else {
        this.sticky_container.addClass('visible');
        this.sticky_toggle.addClass('visible');
    }
};

AnchorMaker.prototype.makeNav = function () {
    var $this = this;
    $('h1:not([id]), h2:not([id]), h3:not([id]), h4:not([id]), h5:not([id]), h6:not([id])').each(function (i, attr) {
        $this.addToSticky(this);
        $(this).attr('id', $this.generateId(this));
    });
};

AnchorMaker.prototype.generateId = function (elem) {
    // todo: refactor duplicate replace to one replace
    // todo: maybe more replaces are needed to build a valid css selector
    return $(elem).text().toLowerCase()
        .replace(new RegExp(' ', 'g'), '-')
        .replace(new RegExp(',', 'g'), '')
        .replace(new RegExp(':', 'g'), '')
        .replace(new RegExp("'", 'g'), '')
        .replace(new RegExp("`", 'g'), '')
        .replace(new RegExp("´", 'g'), '')
        .replace(new RegExp("°", 'g'), '')
        .replace(new RegExp("~", 'g'), '')
        .replace(new RegExp('"', 'g'), '')
        .replace(/\\/, '')
        .substr(0, 25);
};

AnchorMaker.prototype.scrollHandler = function (elem) {
    elem = $(elem);
    if (this.headline.indexOf(elem.localName)) {
        elem.addClass('anchor-maker--highlight');
        setTimeout(function () {
            elem.removeClass('anchor-maker--highlight');
        }, 750);
    }
};

AnchorMaker.prototype.addToSticky = function (elem) {
    var j_elem = $(elem);
    var name = (j_elem.text().length > 40 ? j_elem.text().substr(0, 40) + '..' : j_elem.text());
    this.sticky_container.append($('<a href="#' + encodeURI(this.generateId(elem)) + '" class="element-' + elem.localName + '">' + name + '</a>'));
};