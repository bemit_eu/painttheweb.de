$(function () {
    //Prism.highlightAll();
    var am = new AnchorMaker();
    am.exec();
    hljs.initHighlightingOnLoad();

    if (window.location.hash) {
        var target = $('[id=' + window.location.hash.slice(1) + ']');
        am.scrollHandler(target);
        $('html, body').animate({
            scrollTop: target.offset().top
        }, 650);
    }

    // smooth scroller on all pure target links but not only target (as interference problem with legacy modules)
    $('a[href*="#"]:not([href="#"])').click(function () {
        if (window.location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && window.location.hostname === this.hostname) {
            // creates the target element out of the id
            var target = $(this.hash);
            target = target.length ? target : $('[id=' + this.hash.slice(1) + ']');
            am.scrollHandler(target);
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 650);
            }
        }
    });
});

//hljs.initHighlightingOnLoad();