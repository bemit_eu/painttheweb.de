# Mini Blog in PHP with Markdown Content Files

Define routes and the host in [flow.php](flow.php)

Add .md files in [content](content)
    - folder is the section id
    - file is the article id

## Routes

Any routes that have [FactoryMd](service/FactoryMd.php) as a callback, uses automatically the active path to determine the content file. [app/Law](app/Law.php) and [app/Blog](app/Blog.php) are child of FactoryMd.

Route example:
```php
<?php
$host = 'painttheweb.de';

$route_list = [
    'name-of-route' => [
        'path' => '/the-path-you-want/{could-be-with-placeholder}',
        'controller' => function (Painttheweb\Service\Frontend $frontend) {
            $controller = new Painttheweb\Service\FactoryMd($frontend);
            return $controller->response();
        }
    ],
];
```

This will turn `https://painttheweb.de/the-path-you-want/file-name` to the content file path `content/the-path-you-want/file-name.md` and will use this file to transpile into html and into template as `{{ md_content|raw }}` in the file [FactoryMd.twig](view/FactoryMd.twig).

## Content

For each article is visible, the file matching is done from routing - there is no function to hide a page, it needs to be initialized in [blog.php]. But the article need to be initialized to also be available for url generation, indexing and be available for automatic listing.

```text
    Copyright 2017-2018 Michael Becker, mb@project.bemit.eu
```  